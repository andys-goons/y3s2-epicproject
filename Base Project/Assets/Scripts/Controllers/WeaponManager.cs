﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponManager : MonoBehaviour
{
    //--HI NICK!!//
    //--As you will see, this script is uncommented. That is because it is unfinished.--//
    //--It is not used in the project, but I would like to finish it in the future for the release.--//
    //--As a result, it has not been deleted. However, please disregard it, as it is not currently in use.--//

    public GameObject[] weaponList;
    public Text weaponText;
    public GameObject floatingUI;
    public SpriteRenderer floatingUISprite;
    public List<Image> weaponSprites = new List<Image>();
    public float timeStopValue;
    int selectedWeaponIndex;
    int currentWeaponIndex;
    bool timeStop;
    float unscaledTimer;
    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i <= weaponList.Length-1; i++)
        {
            weaponSprites.Add(weaponList[i].GetComponent<Image>());
        }
        floatingUISprite = floatingUI.GetComponent<SpriteRenderer>();
        weaponText.text = weaponList[selectedWeaponIndex].name;
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {

        //Cycle down through the player's current weapons
        if (Input.GetKeyDown("q"))
        {
            unscaledTimer = 0;
            timeStop = true;
            selectedWeaponIndex -= 1;
            if (selectedWeaponIndex < 0)
            {
                selectedWeaponIndex = weaponList.Length-1;
            }
        }

        //Cycle up through the player's current weapons
        if (Input.GetKeyDown("e"))
        {
            unscaledTimer = 0;
            timeStop = true;
            selectedWeaponIndex += 1;
            if (selectedWeaponIndex >= weaponList.Length)
            {
                selectedWeaponIndex = 0;
            }
        }


        if (selectedWeaponIndex != currentWeaponIndex)
        {
            weaponList[currentWeaponIndex].gameObject.SetActive(false);
            weaponList[selectedWeaponIndex].gameObject.SetActive(true);
            currentWeaponIndex = selectedWeaponIndex;
            weaponText.text = weaponList[selectedWeaponIndex].name;
        }

        if (timeStop)
        {
            floatingUISprite.sprite = weaponSprites[selectedWeaponIndex].sprite;
            floatingUI.gameObject.SetActive(true);
            Time.timeScale = 0;
            unscaledTimer += Time.unscaledDeltaTime;
            if(unscaledTimer >= timeStopValue)
            {
                timeStop = false;
                Time.timeScale = 1;
                unscaledTimer = 0;
                floatingUI.gameObject.SetActive(false);
            }
        }

    }
}
