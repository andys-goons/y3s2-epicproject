﻿//Credit to ReCogMission: https://github.com/ReCogMission/FirstTutorials
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelFade : MonoBehaviour
{
    //--HI NICK!!//
    //--As you will see, this script is uncommented. That is because it is unfinished.--//
    //--It is not used in the project, but I would like to finish it in the future for the release.--//
    //--As a result, it has not been deleted. However, please disregard it, as it is not currently in use.--//

    //--In addition, much of the code sound here is not mine. Credit for that goes to ReCogMission: https://github.com/ReCogMission/FirstTutorials

    //public KeyCode key = KeyCode.Space;
    public bool fadeOut;
    public bool fadeIn;
    public float speedScale = 1f;
    //public Color fadeColor = Color.bla;
    // Rather than Lerp or Slerp, we allow adaptability with a configurable curve
    public AnimationCurve Curve = new AnimationCurve(new Keyframe(0, 1),
        new Keyframe(0.5f, 0.5f, -1.5f, -1.5f), new Keyframe(1, 0));
    public bool startFadedOut = false;


    private float alpha = 0f;
    public Image thisPanel;
    public int direction = 0;
    private float time = 0f;

    // Start is called before the first frame update
    void Start()
    {
        if (startFadedOut) alpha = 1f; else alpha = 0f;
        thisPanel = GetComponent<Image>();
        thisPanel.color = new Color(thisPanel.color.r, thisPanel.color.g, thisPanel.color.b, alpha);
    }

    // Update is called once per frame
    void Update()
    {
        if (fadeOut)
        {
            alpha = 0f;
            time = 1f;
            direction = -1;
            fadeOut = false;
        }
        if (fadeIn)
        {
            alpha = 1f;
            time = 0f;
            direction = 1;
            fadeIn = false;
        }
        //if (direction == 0 && fadeOut)
        //{
        //    if (alpha >= 1f) // Fully faded out
        //    {

                
        //    }
        //    else // Fully faded in
        //    {

        //    }

        //}
    }

    public void OnGUI()
    {
        if (direction != 0)
        {
            time += direction * Time.deltaTime * speedScale;
            alpha = Curve.Evaluate(time);
            thisPanel.color = new Color(thisPanel.color.r, thisPanel.color.g, thisPanel.color.b, alpha);
            if (alpha <= 0f || alpha >= 1f) direction = 0;
        }
    }
}
