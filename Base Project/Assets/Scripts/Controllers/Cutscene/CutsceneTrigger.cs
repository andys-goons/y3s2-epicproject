﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CutsceneTrigger : MonoBehaviour
{
    //--HI NICK!!//
    //--As you will see, this script is uncommented. That is because it is unfinished.--//
    //--It is not used in the project, but I would like to finish it in the future for the release.--//
    //--As a result, it has not been deleted. However, please disregard it, as it is not currently in use.--//

    public GameObject player;
    public GameObject machinaCutscene;
    public GameObject cutsceneHolder;
    public GameObject thisText;
    public PanelFade cameraFade;
    bool cutsceneTriggered;
    bool canSkip;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && canSkip == true)
        {
            //Fade screen back
            cameraFade.fadeIn = true;
            thisText.SetActive(false);

            //Return control to player
            StartCoroutine(ReturnControl());
        }
    }

    //When the players is in contact with the cutscene trigger and presses the interact button, the cutscene will start
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player") && !other.isTrigger)
        {
            //Player presses attack button
            if (Input.GetMouseButtonDown(0) && cutsceneTriggered == false)
            {
                cutsceneTriggered = true;

                //Remove control from player
                player.SetActive(false);
                cutsceneHolder = Instantiate(machinaCutscene, player.transform.position, player.transform.rotation);

                //Fade screen to white
                cameraFade.fadeOut = true;

                //Text on screen
                StartCoroutine(DisplayText());

            }

        }

    }

    //Displays the text on the screen; for now, the text is a placeholder
    IEnumerator DisplayText()
    {
        yield return new WaitForSeconds(3);
        thisText.SetActive(true);
        yield return new WaitForSeconds(3);
        canSkip = true;
    }

    //After the screen fades back in, return control to the player
    IEnumerator ReturnControl()
    {
        canSkip = false;
        yield return new WaitForSeconds(3);
        player.SetActive(true);
        Destroy(cutsceneHolder);
    }
}
