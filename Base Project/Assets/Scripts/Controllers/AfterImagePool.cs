﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Instance Pool for after image.
/// </summary>
public class AfterImagePool : MonoBehaviour
{
    [SerializeField]
    private GameObject afterImagePrefab;

    private Queue<GameObject> avaliableObects = new Queue<GameObject>();

    public static AfterImagePool Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
        IncreasePool();
    }

    /// <summary>
    /// Increase pool to 10 or however images you may want.
    /// </summary>
    private void IncreasePool()
    {
        for (int i = 0; i < 10; i++)
        {
            var instanceToAdd = Instantiate(afterImagePrefab);
            instanceToAdd.transform.SetParent(transform);
            AddToPool(instanceToAdd);
        }
    }

    public void AddToPool(GameObject instance)
    {
        instance.SetActive(false);
        avaliableObects.Enqueue(instance);
    }

    /// <summary>
    /// If there is no objects in the current scene then it will increase the pool to the length of the for loop in IncreasePool()
    /// </summary>
    /// <returns>instance</returns>
    public GameObject GetFromPool()
    {
        if (avaliableObects.Count == 0)
        {
            IncreasePool();
        }

        var instance = avaliableObects.Dequeue();
        instance.SetActive(true);
        return instance;
    }
}