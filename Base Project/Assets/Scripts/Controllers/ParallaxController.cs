﻿///////////////////////////////////////////////////////////////////////////////
///Script followed by AdamCYounis' tutorial                                 ///
///https://www.youtube.com/watch?v=tMXgLBwtsvI&t=42s&ab_channel=AdamCYounis ///
///////////////////////////////////////////////////////////////////////////////

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Apply to objects to make them effected by parallax.
/// </summary>
public class ParallaxController : MonoBehaviour
{
    //Auto-grabbed refrence to the main camera
    Camera cam;

    [Tooltip("The main target of the camera. e.g. the player character")]
    public Transform subject;

    Vector2 startPosition;
    float startZ;

    //How far has been travelled since the start. Calculated every time it is accessed!
    Vector2 travel => (Vector2)cam.transform.position - startPosition;

    float distanceFromSubject => transform.position.z - subject.position.z;
    
    //Gets the clipping plane from the camera based on where this object is in relation to the subject. if(distanceFromSubject > 0){farClipPlain} etc...
    float clippingPlane => (cam.transform.position.z + (distanceFromSubject > 0 ? cam.farClipPlane : cam.nearClipPlane));

    //The parallax factor defines how fast it moves with the player
    float parallaxFactor => Mathf.Abs(distanceFromSubject) / clippingPlane;

    void Start()
    {        
        startPosition = transform.position;
        startZ = transform.position.z;
    }

    private void FixedUpdate()
    {
        cam = Camera.main;
    }

    void Update()
    {
        Vector2 newPos = startPosition + travel * parallaxFactor;
        transform.position = new Vector3(newPos.x, (subject.position.y) + 1, startZ);
    }
}
