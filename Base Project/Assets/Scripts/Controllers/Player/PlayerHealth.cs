﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

/// <summary>
/// Put this on the player to track their health. 
/// </summary>
public class PlayerHealth : MonoBehaviour
{
    public int maxHealth;
    public int currentHealth;
    [Tooltip("Defines what counts and 'low health'. Currently only used to play an event on the Juicer.")]
    public int lowHealthThreshold;
    public int enemyLayer;
    public int damageVolumeLayer;
    [Tooltip("The amount of seconds a player has invincibility after taking damage.")]
    public float invincibilitySeconds;
    public bool canDamage;
    public bool isDead;
    public float respawnDelay = 1f;

    private Vector2 respawnPos;

    public SpriteRenderer thisSprite;

    public bool slowTimeOnDamage = false;

    [Header("Death")]
    public Animator machinaAnimator;
    private PlayerAttackController attackController;
    public PlayerController controller;
    public GameObject[] disableOnDeath;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        attackController = GetComponent<PlayerAttackController>();
        controller = GetComponent<PlayerController>();
    }

    public void DecreaseHealth(int decrementAmount)
    {
        currentHealth -= decrementAmount;

        AnalyticsManager.damageTaken += decrementAmount;

        Juicer.Instance.PlayerDamageEffect(GetComponent<Collider2D>(), slowTimeOnDamage);

        if (currentHealth <= 0)
        {
            //Do lots of things because the player is dead
            attackController.attackEnabled = false;
            controller.SlowMoveSpeed(0);

            isDead = true;
            machinaAnimator.SetTrigger("isDead");
            foreach (GameObject i in disableOnDeath)
            {
                i.SetActive(false);
            }
            canDamage = false;
            AnalyticsManager.IncrementDeaths();
            
            StartCoroutine(RespawnDelay());
            thisSprite.color = thisSprite.color / 0.5f;
        }
        else if (currentHealth > 0)
        {
            StartCoroutine(invincibilityTime());
        }
        if (currentHealth <= lowHealthThreshold)
        {
            Juicer.Instance.PlayerNearDeathEffect();
        }

    }

    public void Respawn()
    {
        currentHealth = maxHealth;
        isDead = false;
        transform.position = respawnPos;
        machinaAnimator.SetTrigger("isRespawned");
        canDamage = true;
        attackController.attackEnabled = true;
        controller.ResetMoveSpeed();
        foreach (GameObject i in disableOnDeath)
        {
            i.SetActive(true);
        }
    }

    public void IncreaseHealth(int incrementAmount)
    {
        currentHealth += Mathf.Clamp(currentHealth + incrementAmount, 0, maxHealth);
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        //Handle colliding with a checkpoint.
        if (other.gameObject.tag == "Checkpoint")
        {
            //Set the new respawn position
            respawnPos = other.transform.position;

            Checkpoint checkpoint = other.gameObject.GetComponent<Checkpoint>();
            //Intermittant checkpoints are only for respawn positions, not for marking new level segments.
            if (checkpoint.intermittantCheckpoint)
            {
                return;
            }
            if (!checkpoint.visited)
            {
                if (!checkpoint.spawnCheckpoint) AnalyticsManager.StartNewLevelSegment(checkpoint.segmentEntering);
                else AnalyticsManager.currentSegment = checkpoint.segmentEntering;
                checkpoint.visited = true;
            }
        }
        //Uh oh! The player hit a damage volume...
        else if (other.gameObject.tag == "DamageVolume")
        {
            DamageVolume dv = other.GetComponent<DamageVolume>();
            if (dv.instantKill)
            {
                currentHealth = 0;
                DecreaseHealth(1);
                AnalyticsManager.deathsByKillVolume++;
            }
            else if (canDamage)
            {
                DecreaseHealth(dv.damagePower);
                if (currentHealth <= 0)
                {
                    AnalyticsManager.deathsByKillVolume++;
                }
            }
        }
    }
    private void OnTriggerStay2D(Collider2D other)
    {
        HandleCollision(other);
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        HandleCollision(other.collider);
    }

    private void HandleCollision(Collider2D other)
    {
        if (other.gameObject.layer == enemyLayer)
        {
            if (canDamage)
            {
                if (other.gameObject.tag == "Enemy") DecreaseHealth(other.gameObject.GetComponent<Enemy>().GetStrength());
                else if (other.gameObject.tag == "Attack") DecreaseHealth(1);
                else if (other.gameObject.tag == "Boss") DecreaseHealth(2);
            }
        }
    }

    //When the player takes damage, they will be invincible for a short amount of time.
    IEnumerator invincibilityTime()
    {
        canDamage = false;
        Physics.IgnoreLayerCollision(gameObject.layer, enemyLayer, true);
        thisSprite.color = thisSprite.color * 0.5f;
        yield return new WaitForSeconds(invincibilitySeconds);
        canDamage = true;
        thisSprite.color = thisSprite.color / 0.5f;
        Physics.IgnoreLayerCollision(gameObject.layer, enemyLayer, false);
    }

    //When the player dies, they will come back after a certain amount of time.
    IEnumerator RespawnDelay()
    {
        isDead = true;
        Juicer.Instance.PlayerDeathEffect(GetComponent<Collider2D>());
        
        yield return new WaitForSeconds(respawnDelay);

        Respawn();
    }

}
