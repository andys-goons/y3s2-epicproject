﻿using UnityEngine;

public class PlayerAfterImage : MonoBehaviour
{
    [SerializeField] private float activeTime = 0.1f;
    private float timeActivated;
    private float alpha;

    [SerializeField] private float alphaSet = 0.8f;
    private float alphaMultiplier = 0.85f;

    private Transform playerObject;

    private SpriteRenderer spriteRenderer;
    private SpriteRenderer playerSpriteRenderer;

    private Color color;

    //Creates an after image on Enable
    private void OnEnable()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        playerObject = GameObject.FindGameObjectWithTag("Player").transform;
        playerSpriteRenderer = playerObject.GetComponentInChildren<SpriteRenderer>();

        alpha = alphaSet;
        spriteRenderer.sprite = playerSpriteRenderer.sprite;
        transform.position = new Vector2(playerObject.position.x, playerObject.position.y + 1);

        if (playerSpriteRenderer.flipX == true)
        {
            spriteRenderer.flipX = true;
        }
        else
            spriteRenderer.flipX = false;

        timeActivated = Time.time;
    }

    //Makes the after image disappear after a few seconds. Variables can be changed in the inspector
    private void Update()
    {

        alpha *= alphaMultiplier;
        color = new Color(1f, 1f, 1f, alpha);
        spriteRenderer.color = color;

        if (Time.time >= (timeActivated + activeTime))
        {
            AfterImagePool.Instance.AddToPool(gameObject);
        }
    }
}