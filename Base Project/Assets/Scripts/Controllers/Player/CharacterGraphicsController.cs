﻿using UnityEngine;

public class CharacterGraphicsController : MonoBehaviour
{
  
    private PlayerController m_player;
    private SpriteRenderer m_spriteRenderer;
    private Animator m_animator;

    private int IsWalkingProperty;
    private int IsGroundedProperty;
    private int IsAttackingProperty;
    private int IsJumpingProperty;
    private int IsFallingProperty;
    private int IsDashingProperty;
    private int AttackComboProperty;

    private PlayerAttackController m_attackController;



    // Set up booleans for the animations controllers
    // Use this for initialization
    private void Start()
    {
    
        m_player = GetComponent<PlayerController>();
        m_spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        m_animator = GetComponentInChildren<Animator>();
        m_attackController = GetComponent<PlayerAttackController>();


        //All properties for the Animator 
        IsAttackingProperty = Animator.StringToHash("isAttack");
        AttackComboProperty = Animator.StringToHash("attackCombo");
        IsWalkingProperty = Animator.StringToHash("isMoving");
        IsGroundedProperty = Animator.StringToHash("isGrounded");
        IsJumpingProperty = Animator.StringToHash("isJumping");
        IsFallingProperty = Animator.StringToHash("isFalling");
        IsDashingProperty = Animator.StringToHash("isDashing");
    }

    // Update is called once per frame
    private void Update()
    {

        //Reads and sets the conditions in the PlayerController.cs and sets the animations states
        if (m_attackController.enabled)
        {            
            m_animator.SetInteger(AttackComboProperty, m_attackController.GetCombo());
            m_animator.SetBool(IsAttackingProperty, m_player.IsAttacking());
        }
        m_animator.SetBool(IsWalkingProperty, m_player.IsWalking());
        m_animator.SetBool(IsGroundedProperty, m_player.IsGrounded());
        m_animator.SetBool(IsJumpingProperty, m_player.IsJumping());
        m_animator.SetBool(IsFallingProperty, m_player.IsFalling());
        m_animator.SetBool(IsDashingProperty, m_player.IsDashing());


        switch (m_player.GetFacingDirection())
        {
            case PlayerController.FacingDirection.Left:
                m_spriteRenderer.flipX = true;
                break;

            case PlayerController.FacingDirection.Right:
            default:
                m_spriteRenderer.flipX = false;
                break;
        }

    }
}