﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A script to attach to the player to allow them to attack. You can just disable the script when you don't want the player to attack. 
/// </summary>
[RequireComponent(typeof(PlayerController))]
public class PlayerAttackController : MonoBehaviour
{
    [Tooltip("These GameObjects will be enabled/disabled when the player attacks. The longer the list, the greater potential combo.")]
    [SerializeField] private GameObject[] slashesGameObjects;

    [Tooltip("The GameObject containing the slash objects. Used when the player flips around.")]
    [SerializeField] private GameObject slashesRoot;

    [Tooltip("The amount of time the player can wait between attacks to continue the combo.")]
    [SerializeField] private float comboCooldown = 0.45f;

    [Tooltip("The amount of time the slash object stays enabled.")]
    [SerializeField] private float slashDuration = 0.3f;

    [Tooltip("The amount of time the player must wait between attacks.")]
    [SerializeField] private float slashCooldown = 0.31f;

    [Tooltip("The player's max horizontal speed will be multiplied by this number while attacking. 0 freezes the player, 1 allows them to run at full speed.")]
    [Range(0,1)][SerializeField] private float attackSlowFactor = 0.2f;

    public bool attackEnabled = false;

    private float slashCooldownTimer;
    private float comboCooldownTimer;
    private int currentCombo;

    private GameObject currentSlash;
    private float slashDurationTimer;

    private PlayerController pc;

    private void Awake()
    {
        pc = GetComponent<PlayerController>();
    }
    private void Start()
    {
        foreach (GameObject slash in slashesGameObjects)
        {
            slash.SetActive(false);
        }
    }


    private void Update()
    {
        //Change the scale to alter the direction the slashes apear in.
        switch (pc.GetFacingDirection())
        {
            case PlayerController.FacingDirection.Left:
                slashesRoot.transform.localScale = new Vector3(-1, slashesRoot.transform.localScale.y, slashesRoot.transform.localScale.z);
                break;

            case PlayerController.FacingDirection.Right:
            default:
                slashesRoot.transform.localScale = new Vector3(1, slashesRoot.transform.localScale.y, slashesRoot.transform.localScale.z);
                break;
        }

        //When the player presses the attack button...
        if (attackEnabled && ShovelKnightInput.WasAttackPressed() && slashCooldownTimer <= 0)
        {
            //Reset the combo counter if it goes beyond the slas object set.
            if (currentCombo > slashesGameObjects.Length - 1)
            {
                currentCombo = 0;
                AnalyticsManager.perSessionMaxCombos++;
            }

            AnalyticsManager.attacks++;

            //Activate the slash
            slashDurationTimer = slashDuration;
            currentSlash = slashesGameObjects[currentCombo];
            currentSlash.SetActive(true);
            pc.SlowMoveSpeed(attackSlowFactor);

            Juicer.Instance.SlashEffect(currentCombo);
            currentCombo++;
            comboCooldownTimer = comboCooldown;
            slashCooldownTimer = slashCooldown;
        }

        //Count the timer values
        if (comboCooldownTimer > 0)
        {
            comboCooldownTimer -= Time.deltaTime;
        }
        else if (comboCooldownTimer <= 0)
        {
            currentCombo = 0;
        }

        if (slashCooldownTimer > 0)
        {
            slashCooldownTimer -= Time.deltaTime;
        }
        if(slashDurationTimer > 0)
        {
            slashDurationTimer -= Time.deltaTime;
        } else if(currentSlash != null)
        {
            currentSlash.SetActive(false);
            currentSlash = null;
            pc.ResetMoveSpeed();
        }
    }

    public int GetCombo()
    {
        return currentCombo;
    }
}
