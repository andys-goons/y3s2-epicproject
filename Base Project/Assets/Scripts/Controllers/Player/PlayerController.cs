﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    ///Variables for all the jumping in the game
    [Header("Jump Variables")]
    [SerializeField] private float m_jumpApexHeight;
    [SerializeField] private float m_jumpApexTime;
    [SerializeField] private float m_terminalVelocity;
    [SerializeField] private float m_coyoteTime;
    [SerializeField] private float m_jumpBufferTime;

    private float currentJumpVelocity;
    private float gravity;
    private float m_jumpBufferTimer;
    private float m_coyoteTimer;

    ///Variables for all the movement in the game
    [Header("Movement Variables")]
    [SerializeField] private float m_accelerationTimeFromRest;
    [SerializeField] private float m_decelerationTimeToRest;
    [SerializeField] private float m_maxHorizontalSpeed;
    [SerializeField] private float m_accelerationTimeFromQuickturn;
    [SerializeField] private float m_decelerationTimeFromQuickturn;

    private float currentSpeed;
    private float movementVelocity;
    private float acceleration;
    private float deceleration;
    private float minJumpVelocity;
    private bool m_cornerCorrect;
    private float m_originalMaxHorizontalSpeed;

    ///Corner correction variables
    [Header("Corner Correction")]
    [SerializeField] private float m_topcastLength;
    [SerializeField] private Vector3 m_edgecastOffset;
    [SerializeField] private Vector3 m_innercastOffset;

    ///Variables for all the dashing in the game in the game
    [Header("Dashing Variables")]
    [SerializeField] private float dashTime;
    [SerializeField] private float dashSpeed;
    [SerializeField] private float distanceBetweenImages;
    [SerializeField] private float dashDeaccelerationTime;
    [SerializeField] private float gDashDeaccelerationTime;

    private float dashDeacelleration;
    private float dashSpeedX;
    private float dashSpeedY;

    private float dashTimeLeft;
    private float lastImageX;
    private float lastImageY;
    private bool canMove = true;
    private bool endOfDash;
    private float endOfDashtimer;
    private bool canDash;
    private bool isDashing;
    public bool enableDash;
    public Transform Sprite;

    [SerializeField] private float ghostDelay;
    private float ghostDelaySeconds;
    public GameObject ghost;
    private bool makeGhost;
    private Vector2 xOffset;
    public GameObject cloneHolder;
    GameObject hairChild;

    [SerializeField] private float Stretch = 0.1f;

    private Rigidbody2D _rigidbody;
    private Transform _squashParent;
    private Vector3 _originalScale;

    ///Variables for Input and other constanct checks
    public enum FacingDirection { Left, Right }

    private Vector3 directionInput;
    private bool grounded;

    private void Start()
    {
        ghostDelaySeconds = ghostDelay;
        m_originalMaxHorizontalSpeed = m_maxHorizontalSpeed;

        _rigidbody = GetComponent<Rigidbody2D>();
        _squashParent = new GameObject(string.Format("_squash_{0}", name)).transform;
        _originalScale = Sprite.transform.localScale;
        hairChild = this.transform.GetChild(3).gameObject;
    }


    /// <summary>
    /// Method that determins whether or not there is input for the player only covers x-direction
    /// </summary>
    /// <returns>true or false</returns>
    public bool IsWalking()
    {
        Vector2 input = ShovelKnightInput.GetDirectionalInput();
        if (Mathf.Abs(input.x) > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool IsAttacking()
    {
        if (ShovelKnightInput.WasAttackPressed())
            return true;
        else
            return false;
    }

    public bool IsJumping()
    {
        if (currentJumpVelocity > 0)
            return true;
        else
            return false;
    }

    public bool IsFalling()
    {
        if (currentJumpVelocity < 0)
            return true;
        else
            return false;
    }

    public bool IsDashing()
    {
        if (isDashing)
            return true;
        else
            return false;
    }



    /// <summary>
    /// Update function.
    /// </summary>
    public void Update()
    {
        //CheckCollisions();
        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
        m_jumpBufferTimer -= Time.deltaTime;
        m_coyoteTimer -= Time.deltaTime;

        //Saves input from player Input.
        if ((Mathf.Abs(ShovelKnightInput.GetDirectionalInput().x) > 0 && canMove))
        {
            directionInput.x = ShovelKnightInput.GetDirectionalInput().x;
        }

        if ((Mathf.Abs(ShovelKnightInput.GetDirectionalInput().y) > 0 && canMove))
        {
            directionInput.y = ShovelKnightInput.GetDirectionalInput().y;
        }


        // Controls the hair positions and direction
        switch (GetFacingDirection())
        {
            case FacingDirection.Left:
                
                if (IsWalking())
                {
                    hairChild.transform.localPosition = new Vector2(0.159f, 1.4f);
                }
                else
                {
                    hairChild.transform.localPosition = new Vector2(0.159f, 1.555f);
                }
                break;

            case FacingDirection.Right:
            default:
                if (IsWalking())
                {
                    hairChild.transform.localPosition = new Vector2(-0.259f, 1.4f);
                }
                else
                {
                    hairChild.transform.localPosition = new Vector2(-0.259f, 1.555f);
                }
                break;
        }

        //Ground check also coyote time check
        if (IsGrounded())
        {
            grounded = true;
            m_coyoteTimer = m_coyoteTime;
        }

        //Checks for jump input also jump buffer check
        if (ShovelKnightInput.IsJumpPressed())
        {
            grounded = false;
            m_jumpBufferTimer = m_jumpBufferTime;
        }

        //Variable jump input
        if (ShovelKnightInput.IsJumpPressed() == false)
        {
            if (GetComponent<Rigidbody2D>().velocity.y > minJumpVelocity)
            {
                currentJumpVelocity = minJumpVelocity;
                grounded = true;
            }
        }

        //Edge case for the dash if grounded. If the player is grounded the player cannot dash downward.
        if (grounded && ShovelKnightInput.GetDirectionalInput().y < 0)
        {
            canDash = false;
        }
      
        //Calls the method to start the dash within the parameter of the dash variables
        if (Input.GetButtonDown("Dash") && canDash && enableDash)
        {
            AttemptToDash();
        }

        //Ground check
        if (!IsGrounded())
        {
            grounded = false;
        }

        //Corner correction when the player hits the corner of a platform
        if (m_cornerCorrect)
        {
            CornerCorrection(GetComponent<Rigidbody2D>().velocity.y);
        }

        // Creates Ghost effect upon dashing
        if (makeGhost)
        {
            if (ghostDelaySeconds > 0)
            {
                ghostDelaySeconds -= Time.deltaTime;
            }
            else
            {
                //Generate Ghost
                GameObject currentGhost = Instantiate(ghost, (Vector2)transform.position + GetComponent<BoxCollider2D>().offset + xOffset, transform.rotation);
                Sprite currentSprite = _squashParent.GetComponentInChildren<SpriteRenderer>().sprite;
                currentGhost.transform.SetParent(cloneHolder.transform);

                switch (GetFacingDirection())
                {
                    case FacingDirection.Left:
                        currentGhost.transform.localScale = new Vector3(-1,1,1);
                        if (_rigidbody.velocity.x < 0)
                        {
                            xOffset = new Vector2(0.5f, 0);
                        }
                        break;

                    case FacingDirection.Right:
                    default:
                        currentGhost.transform.localScale = new Vector3(1,1,1);
                        if (_rigidbody.velocity.x > 0)
                        {
                            xOffset = new Vector2(-0.5f, 0);
                        }
                        break;
                }

                currentGhost.GetComponent<SpriteRenderer>().sprite = currentSprite;
                Destroy(currentGhost, 1f);
                ghostDelaySeconds = ghostDelay;
            }
        }
    }

    //Sets the paramters for the dash also calls from the pool for after Images to create
    private void AttemptToDash()
    {
        directionInput.x = ShovelKnightInput.GetDirectionalInput().x;
        directionInput.y = ShovelKnightInput.GetDirectionalInput().y;

        if(directionInput.x == 0 && directionInput.y == 0)
        {
            isDashing = false;
        }else
        {
            AnalyticsManager.dashes++;
            canDash = false;
            grounded = false;
            isDashing = true;
            makeGhost = true;
            canMove = false;
            dashTimeLeft = dashTime;
        }

        lastImageX = transform.position.x;
        lastImageY = transform.position.y;
    }

    /// <summary>
    /// Corner correction method.
    /// This method gets the player position and raycasts within a certain threshhold from the edge of the character.
    /// </summary>
    /// <param name="yVelocity"></param>
    private void CornerCorrection(float yVelocity)
    {
        /*Vector3 offset = new Vector2(0, 0.5f);
        //Left cast to push the player to the left
        //returns true or false
        RaycastHit2D hit = Physics2D.Raycast(transform.position - m_innercastOffset + Vector3.up * m_topcastLength, Vector3.left, 1f, LayerMask.GetMask("Ground"));
        if (hit.collider != null)
        {
            float newPos = Vector3.Distance(new Vector3(hit.point.x, transform.position.y, 0f) + Vector3.up * m_topcastLength, transform.position - m_edgecastOffset + Vector3.up * m_topcastLength);
            transform.position = new Vector3(transform.position.x + newPos, transform.position.y, transform.position.z);
            GetComponent<Rigidbody2D>().velocity = new Vector2(movementVelocity * 3, yVelocity);
            return;
        }

        //right cast to push the player to the right
        //returns true or false
        hit = Physics2D.Raycast(transform.position + m_innercastOffset + Vector3.up * m_topcastLength, Vector3.right, 1f, LayerMask.GetMask("Ground"));
        if (hit.collider != null)
        {
            float newPos = Vector3.Distance(new Vector3(hit.point.x, transform.position.y, 0f) + Vector3.up * m_topcastLength, transform.position + m_edgecastOffset + Vector3.up * m_topcastLength);
            transform.position = new Vector3(transform.position.x - newPos, transform.position.y, transform.position.z);
            GetComponent<Rigidbody2D>().velocity = new Vector2(movementVelocity * 3, yVelocity);
            return;
        }*/
    }

    //Raycast for both positions on the left and the right
    private void CheckCollisions()
    {
        m_cornerCorrect = Physics2D.Raycast(transform.position + m_edgecastOffset, Vector2.up, m_topcastLength, LayerMask.GetMask("Ground")) &&
                         !Physics2D.Raycast(transform.position + m_innercastOffset, Vector2.up, m_topcastLength, LayerMask.GetMask("Ground")) ||
                         Physics2D.Raycast(transform.position - m_edgecastOffset, Vector2.up, m_topcastLength, LayerMask.GetMask("Ground")) &&
                         !Physics2D.Raycast(transform.position - m_innercastOffset, Vector2.up, m_topcastLength, LayerMask.GetMask("Ground"));
    }

    // WallHit check cast 
    public bool WallHit()
    {
        BoxCast(GetComponent<Rigidbody2D>().position + GetComponent<BoxCollider2D>().offset, GetComponent<BoxCollider2D>().size, 0, Vector2.right, movementVelocity * Time.deltaTime + 0.05f, LayerMask.GetMask("Ground"));
        RaycastHit2D hit = Physics2D.BoxCast(GetComponent<Rigidbody2D>().position + GetComponent<BoxCollider2D>().offset, GetComponent<BoxCollider2D>().size, 0, Vector2.right, movementVelocity * Time.deltaTime + 0.05f, LayerMask.GetMask("Ground"));
        if (hit.collider != null)
        {
            if (hit.normal.x < 0 || hit.normal.x > 0)
            {
                return true;
            }
        }
        return false;
    }

    //Checking for grounding the player
    public bool IsGrounded()
    {
        //checks the next frame to see if the player is going to hit the ground if so then grounded returns true
        RaycastHit2D hit = Physics2D.BoxCast(GetComponent<Rigidbody2D>().position + GetComponent<BoxCollider2D>().offset, GetComponent<BoxCollider2D>().size, 0, Vector2.down, -currentJumpVelocity * Time.deltaTime + 0.05f, LayerMask.GetMask("Ground"));

        if (hit.collider != null)
        {
            //roof collision detection
            if (hit.normal.y < 0)
            {
                currentJumpVelocity = 0;
                return false;
            }

            //wall collision detection
            if (hit.normal.x < 0 || hit.normal.x > 0)
            {
                return false;
            }

            //Sets the players position based off wheere the cast hit was
            GetComponent<Rigidbody2D>().position = new Vector2(transform.position.x, hit.point.y + 0.05f);
            currentJumpVelocity = 0;
            canDash = true;
            makeGhost = false;
            grounded = true;
            return true;
        }
        else
            return false;
    }

    //Gets facing direction of the player based on the last known directional input
    public FacingDirection GetFacingDirection()
    {
        if (directionInput.x < 0)
        {
            return FacingDirection.Left;
        }
        else
        {
            return FacingDirection.Right;
        }
    }


    //Squashes the sprite dependent on speed
    private void SquashAndStretch()
    {
        Sprite.parent = transform;
        Sprite.localPosition = new Vector3(0, GetComponent<BoxCollider2D>().offset.y + 0.1f ,0);
        Sprite.localScale = _originalScale;
        Sprite.localRotation = Quaternion.identity;

        _squashParent.localScale = Vector3.one;
        _squashParent.position = transform.position;

        var velocity = _rigidbody.velocity.ToVector3();
        if (velocity.sqrMagnitude > 0.01f)
        {
            _squashParent.rotation = Quaternion.FromToRotation(Vector3.right, velocity);
        }

        var scaleX = 1.0f + (velocity.magnitude * Stretch);
        var scaleY = 1.0f / scaleX;
        Sprite.parent = _squashParent;
        _squashParent.localScale = new Vector3(scaleX, scaleY, 1.0f);
    }

    /// <summary>
    /// Fixed update. (!!! This is where all the math should go !!!)
    /// </summary>

    public void FixedUpdate()
    {

        SquashAndStretch();
        ///Formulas for all things when the player presses the jump button
        if (!IsGrounded())
        {
            gravity = -2 * m_jumpApexHeight / (m_jumpApexTime * m_jumpApexTime);
            minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(gravity) * m_jumpApexHeight / 3);
            currentJumpVelocity = Mathf.Clamp(currentJumpVelocity, -m_terminalVelocity, (2 * m_jumpApexHeight / m_jumpApexTime));
            currentJumpVelocity += gravity * Time.fixedDeltaTime;
        }

        ///Fast falling
        //if (!IsGrounded() && ShovelKnightInput.GetDirectionalInput().y < 0 && currentJumpVelocity < 0.3f )
        //{
        //    gravity = -2 * m_jumpApexHeight / (m_fastTime * m_fastTime);
        //    currentJumpVelocity = Mathf.Clamp(currentJumpVelocity, -m_fastSpeedTerminal, (2 * m_jumpApexHeight / m_fastTime));
        //    currentJumpVelocity += gravity * Time.deltaTime;
        //}

        ///Coyote time and jumpbuffer calculations
        if (m_jumpBufferTimer > 0 && m_coyoteTimer > 0)
        {
            AnalyticsManager.jumps++;
            Juicer.Instance.PlayerJumpEffect(transform.position);
            m_coyoteTimer = 0;
            m_jumpBufferTimer = 0;
            currentJumpVelocity = (2 * m_jumpApexHeight / m_jumpApexTime);
        }

        /// Horizontal movement. Deaccleration and acceleration as well as horizontal termnial speed.
        if (Mathf.Abs(ShovelKnightInput.GetDirectionalInput().x) > 0)
        {
            acceleration = m_maxHorizontalSpeed / m_accelerationTimeFromRest * Time.fixedDeltaTime;
            currentSpeed += acceleration;
            currentSpeed = Mathf.Clamp(currentSpeed, 0, m_maxHorizontalSpeed);
            movementVelocity = currentSpeed * directionInput.x;
        }
        else
        {
            deceleration = m_maxHorizontalSpeed / m_decelerationTimeToRest * Time.fixedDeltaTime;
            currentSpeed -= deceleration;
            currentSpeed = Mathf.Clamp(currentSpeed, 0, m_maxHorizontalSpeed);
            movementVelocity = currentSpeed * directionInput.x;
        }

        ///Dashing logic and parameters
        if (isDashing)
        {
            if (dashTimeLeft > 0)
            {
                dashSpeedX = directionInput.x * dashSpeed;
                dashSpeedY = directionInput.y * dashSpeed;
                GetComponent<Rigidbody2D>().velocity = new Vector2(dashSpeedX, dashSpeedY);
                currentJumpVelocity = dashSpeedY;
                movementVelocity = dashSpeedX;
                dashTimeLeft -= Time.fixedDeltaTime;

                if ((Mathf.Abs(gameObject.transform.position.x - lastImageX) < distanceBetweenImages) || (Mathf.Abs(gameObject.transform.position.y - lastImageY) < distanceBetweenImages))
                {
                    lastImageX = transform.position.x;
                    lastImageY = transform.position.y;
                }
            }
            if (dashTimeLeft <= 0 || grounded)
            {
                endOfDash = true;
                endOfDashtimer = dashDeaccelerationTime;
                isDashing = false;
                canMove = true; 
                dashSpeedY = 0;
            }
        }
        else if (endOfDash)
        {
            endOfDashtimer -= Time.fixedDeltaTime;
            if (endOfDashtimer >= 0)
            {
                dashDeacelleration = dashSpeedX / dashDeaccelerationTime * Time.fixedDeltaTime;
                dashSpeedX -= dashDeacelleration;
                movementVelocity = Mathf.Abs(dashSpeedX) * directionInput.x;

               
                if (directionInput.x == 1 && directionInput.y == 1 || directionInput.x == -1 && directionInput.y == 1 || directionInput.y == 1)
                {
                    dashDeacelleration = dashSpeedX / dashDeaccelerationTime * 4 * Time.fixedDeltaTime;
                    dashSpeedX -= dashDeacelleration;
                    movementVelocity = Mathf.Abs(dashSpeedX) * directionInput.x;

                    if (currentJumpVelocity < 0)
                    {
                        acceleration = m_maxHorizontalSpeed / m_accelerationTimeFromRest * Time.fixedDeltaTime;
                        currentSpeed += acceleration;
                        currentSpeed = Mathf.Clamp(currentSpeed, 0, m_maxHorizontalSpeed / 2);
                        movementVelocity = Mathf.Abs(currentSpeed) * directionInput.x;
                    }
                }

                if (IsGrounded())
                {
                    dashDeacelleration = dashSpeedX / gDashDeaccelerationTime * Time.fixedDeltaTime;
                    dashSpeedX -= dashDeacelleration;
                    movementVelocity = Mathf.Abs(dashSpeedX) * directionInput.x;
                    if (endOfDashtimer > gDashDeaccelerationTime || dashSpeedX < 6)
                    {
                        endOfDash = false;
                    }
                }
            }
            else
            {
                endOfDash = false;
            }
        }

        if (WallHit())
        {
            movementVelocity = 0;
            endOfDashtimer = 0;
            dashTimeLeft = 0;
            endOfDash = false;
            canMove = true;
        }



  
        ///applying the different speeds when certain paramerters are met
        if (!grounded && !isDashing)
            GetComponent<Rigidbody2D>().velocity = new Vector2(movementVelocity, currentJumpVelocity);
        else if (!isDashing || endOfDash)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(movementVelocity, 0f);
        }
    }

    /// <summary>
    /// Everything past here is for visualizing raycasts and other casts.
    /// </summary>

    //Draws the cornercorrection gizmo
    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position + m_edgecastOffset, transform.position + m_edgecastOffset + Vector3.up * m_topcastLength);
        Gizmos.DrawLine(transform.position - m_edgecastOffset, transform.position - m_edgecastOffset + Vector3.up * m_topcastLength);
        Gizmos.DrawLine(transform.position + m_innercastOffset, transform.position + m_innercastOffset + Vector3.up * m_topcastLength);
        Gizmos.DrawLine(transform.position - m_innercastOffset, transform.position - m_innercastOffset + Vector3.up * m_topcastLength);

        Gizmos.DrawLine(transform.position - m_innercastOffset + Vector3.up * m_topcastLength,
                        transform.position - m_innercastOffset + Vector3.up * m_topcastLength + Vector3.left * m_topcastLength);
        Gizmos.DrawLine(transform.position + m_innercastOffset + Vector3.up * m_topcastLength,
                        transform.position + m_innercastOffset + Vector3.up * m_topcastLength + Vector3.right * m_topcastLength);
    }

    //Draws the boxcast for grounded check
    static public RaycastHit2D BoxCast(Vector2 origen, Vector2 size, float angle, Vector2 direction, float distance, int mask)
    {
        RaycastHit2D hit = Physics2D.BoxCast(origen, size, angle, direction, distance, mask);

        //Setting up the points to draw the cast
        Vector2 p1, p2, p3, p4, p5, p6, p7, p8;
        float w = size.x * 0.5f;
        float h = size.y * 0.5f;
        p1 = new Vector2(-w, h);
        p2 = new Vector2(w, h);
        p3 = new Vector2(w, -h);
        p4 = new Vector2(-w, -h);

        Quaternion q = Quaternion.AngleAxis(angle, new Vector3(0, 0, 1));
        p1 = q * p1;
        p2 = q * p2;
        p3 = q * p3;
        p4 = q * p4;

        p1 += origen;
        p2 += origen;
        p3 += origen;
        p4 += origen;

        Vector2 realDistance = direction.normalized * distance;
        p5 = p1 + realDistance;
        p6 = p2 + realDistance;
        p7 = p3 + realDistance;
        p8 = p4 + realDistance;

        //Drawing the cast
        Color castColor = hit ? Color.red : Color.yellow;
        Debug.DrawLine(p1, p2, castColor);
        Debug.DrawLine(p2, p3, castColor);
        Debug.DrawLine(p3, p4, castColor);
        Debug.DrawLine(p4, p1, castColor);

        Debug.DrawLine(p5, p6, castColor);
        Debug.DrawLine(p6, p7, castColor);
        Debug.DrawLine(p7, p8, castColor);
        Debug.DrawLine(p8, p5, castColor);

        Debug.DrawLine(p1, p5, Color.grey);
        Debug.DrawLine(p2, p6, Color.grey);
        Debug.DrawLine(p3, p7, Color.grey);
        Debug.DrawLine(p4, p8, Color.grey);
        if (hit)
        {
            Debug.DrawLine(hit.point, hit.point + hit.normal.normalized * 0.2f, Color.yellow);
        }

        return hit;
    }


    // Controls Dash for early tutorial stage 
    public void SetDashEnabled(bool dash)
    {
        enableDash = dash;
    }

    // Slows down move speed when player attacks
    public void SlowMoveSpeed(float factor)
    {
        Debug.Log("Slowing move speed!");
        m_maxHorizontalSpeed *= factor;
    }

    // Resets movespeed once the duration is over 
    public void ResetMoveSpeed()
    {
        Debug.Log("Resetting move speed");
        m_maxHorizontalSpeed = m_originalMaxHorizontalSpeed;
    }
}