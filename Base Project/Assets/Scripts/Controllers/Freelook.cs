﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Freelook : MonoBehaviour
{
    //--HI NICK!!//
    //--As you will see, this script is uncommented. That is because it is unfinished.--//
    //--It is not used in the project, but I would like to finish it in the future for the release.--//
    //--As a result, it has not been deleted. However, please disregard it, as it is not currently in use.--//

    public CinemachineBrain thisBrain;
    Vector3 VCamTransform;

    public CinemachineTransposer thisTransposer;
    public Vector3 FollowOffset;
    public Vector3 originalOffset;
    // Start is called before the first frame update
    void Start()
    {
        thisBrain = GetComponent<CinemachineBrain>();
        
    }

    // Update is called once per frame
    void Update()
    {
        FollowOffset = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), -5) * 5;
        if (Input.GetKeyDown(KeyCode.Z))
        {
            originalOffset = thisBrain.ActiveVirtualCamera.VirtualCameraGameObject.GetComponent<CinemachineVirtualCamera>().GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset;
        }
        if (Input.GetKey(KeyCode.Z))
        {
            thisTransposer = thisBrain.ActiveVirtualCamera.VirtualCameraGameObject.GetComponent<CinemachineVirtualCamera>().GetCinemachineComponent<CinemachineTransposer>();
            
            thisTransposer.m_FollowOffset = originalOffset + FollowOffset;
        }
        else
        {
            thisTransposer.m_FollowOffset = originalOffset;
        }
    }
}
