﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using UnityEngine.UIElements;

public class HealthBar : MonoBehaviour
{
    public Slider healthBar;
    public PlayerHealth thisHealth;

    //Set the health bar's max health on the slider to be the same as the player's max health.
    void Start()
    {
        healthBar = GetComponent<Slider>();
        healthBar.maxValue = thisHealth.maxHealth;
        
    }

    //The health bar displays the player's current health.
    void Update()
    {
        healthBar.value = thisHealth.currentHealth;
    }
}
