﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    //If player clicks on button it will send them to the main game
    public void StartGame()
    {
        if (MusicManager.instance != null) MusicManager.instance.PlayClickSound();
        SceneManager.LoadScene(1); 
    }

    //If player clicks on exit button it will quit the game
    public void ExitGame()
    {
        if (MusicManager.instance != null) MusicManager.instance.PlayCancelSound();
        Application.Quit();
    }

    public void TitleScreen()
    {
        if (MusicManager.instance != null) MusicManager.instance.PlayClickSound();
        SceneManager.LoadScene(0);
    }
   
}
