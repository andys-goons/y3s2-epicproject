﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class EnemyController : DestructableObject
{
    //The 'Statistics' header is continued from DestructableObject
    [Tooltip("How strong the enemy is. Directly effects how much damage it does.")]
    [SerializeField] private int strength;
    [Tooltip("The amount of units the enemy can see.")]
    [SerializeField] private float sightRange;

    [Header("Logic & AIs")]
    [Tooltip("Set to true if the enemy is flying. Flying enemies will ignore regular controller components.")]
    [SerializeField] private bool isFlying = false;
    [Tooltip("How fast the enemy moves in units per second. Only used if isFlying is true.")]
    [SerializeField] private float moveSpeed;
    [Tooltip("How quickly the enemy speeds up towards the target. Only used if isFlying is true")]
    [SerializeField] private float acceleration;

    [Tooltip("Set to true to make the enemy chase the target on the ground. Uses EnemyPhysicsController. Needs a collider and rigidbody 2D")]
    [SerializeField] private bool isGroundChasing = false;

    [Header("Refrences")]
    [Tooltip("Who is this enemy trying to kill? Used mainly when pathing to the target")]
    [SerializeField] public GameObject target;

    private Rigidbody2D rb;
    private Vector2 directionalInput;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();

    }

    private void Start()
    {
        if (isFlying)
        {
            rb.gravityScale = 0;
        }
    }

    public void Update()
    {
        //Flying enemy logic. Dumbly floats towards the target.
        if (isFlying)
        {
            //Get the direction to the target
            Vector2 vectorToTarget = target.transform.position - transform.position;

            //Accelerate towards the target
            rb.AddForce(vectorToTarget.normalized * acceleration);

            //If going too fast, slow down!
            if(Mathf.Abs(rb.velocity.magnitude) > moveSpeed)
            {
                rb.AddForce(-rb.velocity);
            }
        }

        //Ground chasing logic. Sets input for an attached EnemyPhysicsController
        if (isGroundChasing)
        {
            //AI for following the player
            Vector2 targetPos = target.transform.position;
            if (IsNearby(transform.position.x, targetPos.x, 0.1f))
            {
                directionalInput.x = 0;
            }
            else if (targetPos.x > transform.position.x)
            {
                directionalInput.x = 1;
            }
            else if (targetPos.x < transform.position.x)
            {
                directionalInput.x = -1;
            }
        }
    }

    public Vector2 GetDirectionalInput()
    {
        return directionalInput;
    }

    //Isnearby and its overrides
    private bool IsNearby(Vector2 self, Vector2 target, float leeway)
    {
        return IsNearby(self.x, target.x, leeway) && IsNearby(self.y, target.y, leeway);
    }
    private bool IsNearby(Vector2 self, Vector2 target, Vector2 leeway)
    {
        return IsNearby(self.x, target.x, leeway.x) && IsNearby(self.y, target.y, leeway.y);
    }

    /// <summary>
    /// Returns true if x is within leeway of y
    /// </summary>
    /// <param name="x">The number in question</param>
    /// <param name="y">The target number</param>
    /// <param name="leeway">The radius within y that x has to be</param>
    /// <returns></returns>
    private bool IsNearby(float x, float y, float leeway)
    {
        return (x > y - leeway && x < y + leeway);
    }
}