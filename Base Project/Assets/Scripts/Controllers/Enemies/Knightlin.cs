﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The Knightlin script. Includes the basic AI that runs Knightlins
/// </summary>
public class Knightlin : Enemy
{
    //The different states Knightlins can be in.
    private enum KnightlinState { waiting, shooting, cooling};

    [SerializeField] private float shotDelay = 1.5f;

    [Header("Bullet Information")]
    private float facingDirection;
    [Tooltip("The prefab this Knightlin shoots.")]
    [SerializeField] private GameObject bulletPrefab;
    [Tooltip("How fast the shot accelerates.")]
    [SerializeField] private float shotAcceleration = 10;
    [Tooltip("The max velocty of the shot.")]
    [SerializeField] private float shotVelocity;
    private Vector2 shotDirection;
    [Tooltip("The transform to spawn a shot at. Use this so it doesn't appear in the middle of this object.")]
    [SerializeField] private Transform shotOrigin;

    private KnightlinState state = KnightlinState.cooling;
    private float timer = 0.1f;

    private Animator an;

    private void Awake()
    {
        an = GetComponent<Animator>();
    }

    new protected void FixedUpdate()
    {
        base.FixedUpdate();
        float distanceToPlayer = ((Vector2)transform.position - (Vector2)target.transform.position).magnitude;

        //Make the knightlin always facing the player so it shoots properly.
        if (target.transform.position.x < transform.position.x)
        {
            facingDirection = -1;
            shotDirection.x = -1;
            transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        }
        else
        {
            facingDirection = 1;
            shotDirection.x = 1;
            transform.localScale = new Vector3(-Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        }

        //Waiting state logic.
        if (state == KnightlinState.waiting)
        {
            if(distanceToPlayer < sightRange)
            {
                timer = shotDelay;
                state = KnightlinState.shooting;
                invulnerable = false;
                an.SetBool("attacking", true);
            }
        }
        //Shooting state logic
        if (state == KnightlinState.shooting)
        {
            if(timer > 0)
            {
                timer -= Time.fixedDeltaTime;
            } else
            {
                if(!string.IsNullOrEmpty(attackSoundEvent)) FMODUnity.RuntimeManager.PlayOneShotAttached(attackSoundEvent, gameObject);
                BulletController tempBullet = Instantiate(bulletPrefab, shotOrigin.transform.position, Quaternion.identity).GetComponent<BulletController>();

                tempBullet.velocity = shotVelocity;
                tempBullet.acceleration = shotAcceleration;
                tempBullet.direction = shotDirection;

                state = KnightlinState.cooling;
                timer = shotDelay;
            }
        }
        //Cooling off after shooting logic
        if (state == KnightlinState.cooling)
        {
            if (timer > 0)
            {
                timer -= Time.fixedDeltaTime;
            }
            else
            {
                state = KnightlinState.waiting;
                invulnerable = true;
                an.SetBool("attacking", false);
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Attack")
        {
            Damage(1);
        }
    }

    //Knightlin handles taking damage a bit differently, so the clash effect can be played when taking damage.
    new public bool Damage(int amount)
    {
        if (invulnerable && damageCooldownTimer <= 0) Juicer.Instance.ClashEffect(GetComponent<Collider2D>(), 1, false);

        return base.Damage(amount);
    }
}
