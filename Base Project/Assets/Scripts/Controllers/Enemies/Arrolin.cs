﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Logic and AI for the Arrolin enemy.
/// </summary>
public class Arrolin : Enemy
{
    private enum ArrolinState { waiting, falling, inGround, returning };

    [Tooltip("The delay (in seconds) before the Arrolin drops down after seeing the player.")]
    [SerializeField] private float dropDelay;
    [Tooltip("The delay (in seconds) before the Arrolin returns to its original position after colliding with an object on the 'Ground' layer.")]
    [SerializeField] private float riseDelay;
    [Tooltip("How fast the arrolin returns to its resting spot.")]
    [SerializeField] private float returnSpeed;
    [Tooltip("How hard/fast the arrolin drops")]
    [SerializeField] private float dropPower;

    [Header("Hovering Effect")]
    [SerializeField] private float maxOffset = 0.1f;
    [SerializeField] private float hoverSpeed = 0.02f;

    private ArrolinState state = ArrolinState.waiting;
    private float riseTimer;
    private float dropTimer;

    [SerializeField] private int playerLayer;

    private Vector2 initialLocation;
    private float offsetStep;

    private Rigidbody2D rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        initialLocation = transform.position; //The arrolin's initial position is its resting place.
    }

    new void FixedUpdate()
    {
        //Waiting for the player to come too close
        if (state == ArrolinState.waiting)
        {
            //rb.gravityScale = 0;
            rb.velocity = new Vector2();
            if (Mathf.Abs(target.transform.position.x - transform.position.x) < sightRange)
            {
                dropTimer = dropDelay;
                state = ArrolinState.falling;
            }

            offsetStep += hoverSpeed;
            if (offsetStep > 9999) offsetStep = 1;
            transform.position = new Vector2(transform.position.x, initialLocation.y + Mathf.Sin(offsetStep) * maxOffset);
        }

        //Logic for mid-fall
        if (state == ArrolinState.falling)
        {
            if (dropTimer > 0)
            {
                dropTimer -= Time.fixedDeltaTime;
            }
            else
            {
                //rb.gravityScale = dropPower;
                rb.velocity = new Vector2(0, -dropPower);
            }
        }

        //For when the arrolin has hit the ground and is cooling down before it begins rising again.
        if (state == ArrolinState.inGround)
        {
            if (riseTimer > 0)
            {
                riseTimer -= Time.fixedDeltaTime;
                rb.velocity = new Vector2();
            }
            else
            {
                state = ArrolinState.returning;
            }
        }

        //Logic for when the arrolin is on its way back to its original location.
        if (state == ArrolinState.returning)
        {
            rb.velocity = (initialLocation - (Vector2)transform.position).normalized * returnSpeed;
            if (((Vector2)transform.position - initialLocation).magnitude < 0.1f)
            {
                state = ArrolinState.waiting;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            riseTimer = riseDelay;
            state = ArrolinState.inGround;
            if (!string.IsNullOrEmpty(attackSoundEvent)) FMODUnity.RuntimeManager.PlayOneShotAttached(attackSoundEvent, gameObject);
        }
    }
}