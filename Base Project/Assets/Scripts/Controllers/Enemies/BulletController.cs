﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A script to attach to bullets. Namely for the knightlin.
/// </summary>
public class BulletController : MonoBehaviour
{
    //NOTE:
    //All of these values are automatically set when this is shot from a Knightlin! See Knightlin's attributes.
    [Tooltip("The direction to travel in.")]
    [SerializeField] public Vector2 direction;
    [Tooltip("How fast to accelerate.")]
    [SerializeField] public float acceleration;
    [Tooltip("The max speed.")]
    [SerializeField] public float velocity;

    private Rigidbody2D rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.gravityScale = 0;

        //Accelerate towards the target
        rb.AddForce(direction.normalized * acceleration);

        //If going too fast, slow down!
        if (Mathf.Abs(rb.velocity.magnitude) > velocity)
        {
            rb.AddForce(-direction.normalized * acceleration);
        }
    }
}
