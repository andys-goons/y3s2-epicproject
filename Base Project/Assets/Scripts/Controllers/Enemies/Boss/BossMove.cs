﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossMove : MonoBehaviour
{
    Rigidbody2D thisRB;

    public Vector3 boxcastOffset;
    public Vector3 boxcastSize;

    public LayerMask groundedMask;

    public float terminalVelocity;
    public float gravity;
    public float initialGravity;
    public float objectSpeed;
    //public float currentSpeed;

    public bool terminalHit;
    public bool grounded;
    public bool castBool;

    public BossController bossController;
    // Start is called before the first frame update
    void Start()
    {
        bossController = GetComponent<BossController>();
        thisRB = GetComponent<Rigidbody2D>();
        gravity = initialGravity;
    }

    
    public void Move()
    {
        float currentSpeed = bossController.speed * transform.forward.magnitude;
        Vector3 previousPosition = thisRB.transform.position;
        float tempGravity;

        //If the boss is grounded, apply gravity of 0. Otherwise, apply normal gravity
        if (CheckIfGrounded())
        {
            terminalHit = false;
            grounded = true;
            tempGravity = 0;
            GetComponent<Animator>().SetTrigger("isLanding");
        }
        else
        {
            grounded = false;
            tempGravity = gravity;
        }

        //Do not allow y velocity to exceed terminal velocity
        if (thisRB.velocity.y < -terminalVelocity)
        {
            terminalHit = true;
        }

        gravityFunction(currentSpeed, tempGravity, previousPosition);

    }

    //Apply gravity to the boss.
    public void gravityFunction(float s, float g, Vector3 p)
    {
        thisRB.velocity = new Vector2(s, thisRB.velocity.y - g);

        if (thisRB.transform.position.y < p.y || terminalHit)
        {
            thisRB.velocity = new Vector2(s, -terminalVelocity);
        }
    }

    //Performs a boxcast at the boss' feet. Returns true if a platform is underneath it.
    bool CheckIfGrounded()
    {
        castBool = Physics2D.BoxCast(thisRB.transform.position - boxcastOffset, boxcastSize * 0.5f, 0, Vector2.down, boxcastSize.y, groundedMask);
        if (castBool)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
