﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossJump : MonoBehaviour
{
    BossController bossController;
    BossMove bossMove;
    // Start is called before the first frame update
    void Start()
    {
        bossController = GetComponent<BossController>();
        bossMove = GetComponent<BossMove>();
    }

    //Jumps when the boss is grounded
    public void jumpFunction()
    {
        //Force the boss upwards when there is an obstacle in its way so that it jumps over it.
        if (ObstacleCheck())
        {
            bossMove.grounded = false;
            bossController.bossRB.velocity = new Vector2(bossController.bossRB.velocity.x, bossController.jumpForce);
            bossMove.gravity = bossMove.initialGravity * bossController.jumpGravity;
            GetComponent<Animator>().SetTrigger("IsJumping");
        }
        //If the boss is not grounded and the boss does not detect anything in front of it, apply gravity.
        if (!ObstacleCheck() && !bossMove.grounded)
        {
            bossMove.gravity = bossMove.initialGravity;

        }

    }

    //Check if there is an obstacle that needs to be jumped over
    bool ObstacleCheck()
    {
        return Physics2D.Raycast(transform.position, -transform.right, bossController.objectDetectionDistance, bossMove.groundedMask);
    }
}
