﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossShoot : MonoBehaviour
{
    public bool timerBool;

    public float bulletSpeed;
    public float fullTimer;
    public float fireTimer;

    public GameObject bulletPrefab;

    public int currentBullet = 0;

    public List<GameObject> bulletList;

    public Vector3 rotationMod;
    Vector3 rmOrig;

    void Start()
    {
        rmOrig = rotationMod;
    }

    private void Update()
    {
        //Run timer when bool is true
        if (timerBool)
        {
            shootTimerFunction(0.5f, 0.1f);
        }
    }

    //If the timer is running, fire a bullet whenever fireTimer exceeds fireTime, and return to walking whenever fullTimer exceeds maxTime.
    void shootTimerFunction(float maxTime, float fireTime)
    {
        fullTimer += Time.deltaTime;
        fireTimer += Time.deltaTime;
        if (fireTimer >= fireTime)
        {
            InstantiateBullet();
            fireTimer = 0;
        }
        if(fullTimer >= maxTime)
        {
            fullTimer = 0;
            timerBool = false;
            rotationMod = rmOrig;
        }
    }

    //Instantiate a bullet and add it to the list. Set the velocity of the bullet and increase the angle at which a bullet is fired whenever it fires one, so that they are fired at different angles.
    void InstantiateBullet()
    {
        bulletList.Add(Instantiate(bulletPrefab, transform.position, Quaternion.Euler(transform.eulerAngles + rotationMod)));
        bulletList[currentBullet].GetComponent<Rigidbody2D>().velocity = bulletSpeed * -bulletList[currentBullet].transform.right;
        currentBullet++;
        rotationMod += rmOrig;
    }
}
