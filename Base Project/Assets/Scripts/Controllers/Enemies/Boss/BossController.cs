﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour
{
    public BossJump bossJump;
    public BossMove bossMove;
    public BossShoot bossShoot;

    public bool canJump;
    public bool bossShooting;

    public float speed;
    public float originalSpeed;
    public float jumpGravity;
    public float jumpForce;
    public float objectDetectionDistance;

    public int playerLayer;

    public LayerMask enemyMask;

    public Rigidbody2D bossRB;

    public string PlatformTag;
    public string wallTag;
    // Start is called before the first frame update
    void Start()
    {
        //Get a reference to all necessary components
        bossRB = GetComponent<Rigidbody2D>();
        bossJump = GetComponent<BossJump>();
        bossMove = GetComponent<BossMove>();
        bossShoot = GetComponent<BossShoot>();

        canJump = true;

    }


    private void FixedUpdate()
    {
        
        bossJump.jumpFunction();
        bossMove.Move();
        //Create a list to contain the bullets
        for (int i = 0; i < 5; i++)
        {
            if (bossShoot.bulletList[i] == null)
            {
                bossShoot.bulletList.RemoveAt(i);
                bossShoot.currentBullet--;
            }
        }

    }
}
