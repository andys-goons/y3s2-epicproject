﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTrigger : MonoBehaviour
{
    public GameObject boss;
    public GameObject doorCollider;
    public GameObject bossHealth;

    //When the trigger is entered for the first time, activate the boss battle.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!boss.activeInHierarchy)
        {
            boss.SetActive(true);
            doorCollider.SetActive(true);
            bossHealth.SetActive(true);
        }
    }
}
