﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetBoss : MonoBehaviour
{
    public Transform bossSpawn;
    public GameObject boss;
    public Enemy healthTracker;

    public GameObject bossWall;
    public GameObject bossHealth;
    private void Start()
    {
        healthTracker = boss.GetComponent<Enemy>();
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            healthTracker.hp = healthTracker.maxHp;
            boss.transform.position = bossSpawn.position;
            boss.transform.rotation = bossSpawn.rotation;
            boss.SetActive(false);
            bossWall.SetActive(false);
            bossHealth.SetActive(false);
        }
    }
}
