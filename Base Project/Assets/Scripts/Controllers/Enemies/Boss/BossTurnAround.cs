﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTurnAround : MonoBehaviour
{
    public GameObject thisCamera;
    public string wallTag;
    BossController bossController;
    BossMove bossMove;
    public BossShoot bossShoot;

    public float time;
    bool timerStart;
    // Start is called before the first frame update
    void Start()
    {
        bossController = GetComponent<BossController>();
        bossMove = bossController.bossMove;
    }
    private void Update()
    {
        //Change the rotation of the boss based on the boss' current speed
        if (bossController.speed > 0)
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }
        if (bossController.speed < 0)
        {
            transform.eulerAngles = Vector3.zero;
        }
        
        //Always add to the current timer to keep track of how long the boss has been standing still.
        if (timerStart)
        {
            timerFunction(1);
        }

    }
    //If the boss collides with something tagged as a wall, it will turn around and shoot a series of bullets. It will not walk during this.
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == wallTag)
        {
            bossController.originalSpeed = bossController.speed;
            bossController.speed = 0;
            transform.eulerAngles += new Vector3(0, 180, 0);
            bossController.bossShooting = true;
            bossShoot.timerBool = true;
            timerStart = true;
            time = 0;

        }
    }
    //When the timer runs out, the boss will continue walking in the other direction.
    void timerFunction(float maxTime)
    {
        time += Time.deltaTime;
        if (time > maxTime)
        {
            bossController.bossShooting = false;
            bossController.speed = bossController.originalSpeed * -1;
        }
    }
}
