﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossHealth : MonoBehaviour
{
    public Slider healthBar;
    public Enemy thisHealth;
    // Start is called before the first frame update
    void Start()
    {
        healthBar = GetComponent<Slider>();
        healthBar.maxValue = thisHealth.maxHp;

    }

    // Update is called once per frame
    void Update()
    {
        healthBar.value = thisHealth.hp;
    }
}
