﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using FMOD.Studio;

/// <summary>
/// A script to manage music through all scenes of the game.
/// </summary>
public class MusicManager : MonoBehaviour
{
    public static MusicManager instance;
    [EventRef]
    public string tutorialMusic;
    [EventRef]
    public string villageMusic;
    [EventRef]
    public string forestMusic;
    [EventRef]
    public string towerMusic;
    [EventRef]
    public string bossMusic;
    [EventRef]
    public string buttonClickSound;
    [EventRef]
    public string cancelClickSound;

    EventInstance music;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        DontDestroyOnLoad(gameObject);
        music = RuntimeManager.CreateInstance(tutorialMusic);
        music.start(); 
    }
    public void PlayNewMusic(string FMODEvent)
    {
        music.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        music = RuntimeManager.CreateInstance(FMODEvent);
        music.start();
    }

    public void PlayTutorialMusic()
    {
        music.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        music = RuntimeManager.CreateInstance(tutorialMusic);
        music.start();
    }
    public void PlayVillageMusic()
    {
        music.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        music = RuntimeManager.CreateInstance(villageMusic);
        music.start();
    }
    public void PlayForestMusic()
    {
        music.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        music = RuntimeManager.CreateInstance(forestMusic);
        music.start();
    }
    public void PlayTowerMusic()
    {
        music.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        music = RuntimeManager.CreateInstance(towerMusic);
        music.start();
    }
    public void PlayBossMusic()
    {
        music.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        music = RuntimeManager.CreateInstance(bossMusic);
        music.start();
    }

    public void PlayClickSound()
    {
        RuntimeManager.PlayOneShot(buttonClickSound);
    }
    public void PlayCancelSound()
    {
        RuntimeManager.PlayOneShot(cancelClickSound);
    }
}
