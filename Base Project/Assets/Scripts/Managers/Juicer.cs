﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using UnityEngine.Events;
using Cinemachine;

public enum ParticleType { smoke, metalClash, strike, blood }

/// <summary>
/// A tasty script to handle most of the juice in the game. It just helps have everything in one place.
/// </summary>
public class Juicer : MonoBehaviour
{
    public static Juicer Instance { get; private set; }

    [SerializeField] private GameObject[] particles;

    private FastPool clashPool;
    private FastPool slashPool;
    private FastPool bloodPool;
    private float slowTimeTimer;

    [Header("Sound Effects")]
    [Header("Player")]
    [EventRef] [SerializeField] private string slashSound;
    private FMOD.Studio.EventInstance[] slashEventInstances;  //Multiple instances are used so that sounds may be overlapped.
    private int currentSlash = 0;

    [EventRef] [SerializeField] private string playerDamageSound;
    [EventRef] [SerializeField] private string playerDeathSound;
    [EventRef] [SerializeField] private string playerNearDeathSound;
    [EventRef] [SerializeField] private string playerJumpSound;
    [EventRef] [SerializeField] private string clashSound;

    public UnityEvent screenShakeEvent;
    public CinemachineImpulseDefinition impulseDef;
    public CinemachineImpulseSource source;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Debug.LogWarning("Warning: multiple " + this + " in scene!");
        }

        clashPool = FastPoolManager.GetPool(particles[GetParticleID(ParticleType.metalClash)]);
        slashPool = FastPoolManager.GetPool(particles[GetParticleID(ParticleType.strike)]);

        slashEventInstances = new FMOD.Studio.EventInstance[5];

        impulseDef.m_RawSignal = source.m_ImpulseDefinition.m_RawSignal;
    }

    private void Start()
    {
        //Initialize the slash sound instances
        for(int i = 0; i < slashEventInstances.Length; i++)
        {
            slashEventInstances[i] = RuntimeManager.CreateInstance(slashSound);
        }
    }

    private void FixedUpdate()
    {
        if(slowTimeTimer > 0)
        {
            slowTimeTimer -= Time.fixedUnscaledDeltaTime;
        } else
        {
            Time.timeScale = 1;
        }
    }

    #region playerEffects

    public void PlayerDamageEffect(Collider2D col, bool slowTime = false)
    {
        impulseDef.m_AmplitudeGain = 1f;
        source.m_ImpulseDefinition = impulseDef;
        screenShakeEvent.Invoke();

        if (playerDamageSound != "") RuntimeManager.PlayOneShot(playerDamageSound);
        if (slowTime) SlowTime(0.1f, 0.2f);
        
        BloodSplatter(col);
    }

    public void PlayerDeathEffect(Collider2D col)
    {
        if (playerDeathSound != "") RuntimeManager.PlayOneShot(playerDeathSound);
    }

    public void PlayerNearDeathEffect()
    {
        if (playerNearDeathSound != "") RuntimeManager.PlayOneShot(playerNearDeathSound);
    }

    public void PlayerJumpEffect(Vector2 position)
    {
        RuntimeManager.PlayOneShot(playerJumpSound);
    }

    #endregion

    #region combatEffects

    public void BloodSplatter(Collider2D col)
    {
       if(bloodPool != null) SummonParticle(ParticleType.blood, RandomPointInBounds(col));
    }


    /// <summary>
    /// Play the slash sound effect. Particles or other effects to be added at a later date.
    /// </summary>
    /// <param name="power">The power of the swing from 0-3. Effects the sound of the slash.</param>
    public void SlashEffect(int power = 1)
    {
        //Set the SwingStrength parameter and play the sound
        slashEventInstances[currentSlash].setParameterByName("SwingStrength", power);
        slashEventInstances[currentSlash].start();

        //Increment the used instance
        currentSlash++;
        if (currentSlash > slashEventInstances.Length-1) currentSlash = 0;
    }

    public void ClashEffect(Collider2D col, float power = 1, bool slowTime = false)
    {
        impulseDef.m_AmplitudeGain = 0.5f;
        source.m_ImpulseDefinition = impulseDef;
        screenShakeEvent.Invoke();
        if (slowTime) SlowTime(0.01f * power);
        SummonParticle(ParticleType.metalClash, RandomPointInBounds(col));
        
        RuntimeManager.PlayOneShot(clashSound);
        
    }

    /// <summary>
    /// Plays a set of effects for striking something.
    /// </summary>
    /// <param name="col">The collider that was hit</param>
    /// <param name="power">How powerful the hit was. Effects screen shake, time slow, etc</param>
    public void StrikeEffect(Collider2D col, float power = 1, bool slowTime = false)
    {
        impulseDef.m_AmplitudeGain = 0.2f;
        source.m_ImpulseDefinition = impulseDef;
        screenShakeEvent.Invoke();
        if (slowTime) SlowTime(0.01f * power);
        SummonParticle(ParticleType.strike, RandomPointInBounds(col));
        
    }
    #endregion

    #region utilities
    /// <summary>
    /// Slows time for a designated amount of IRL seconds.
    /// </summary>
    /// <param name="duration">Duration of the slow in real time seconds</param>
    /// <param name="scale">The scale to set time to. Default is 0.3</param>
    public void SlowTime(float duration, float scale = 0.2f)
    {
        Time.timeScale = scale;
        slowTimeTimer = duration;
    }

    /// <summary>
    /// Summon a particle effect with the given specs
    /// </summary>
    /// <param name="type">The particle type</param>
    /// <param name="position">the position of the particle</param>
    public void SummonParticle(ParticleType type, Vector3 position)
    {
        SummonParticle(GetParticleID(type), position);
    }
    /// <summary>
    /// Summon a particle effect with the given specs
    /// </summary>
    /// <param name="id">The particle's ID</param>
    /// <param name="position">the position of the particle</param>
    public GameObject SummonParticle(int id, Vector3 position)
    {
        GameObject temp = null;

        if (id == 1)
        {
            temp = clashPool.FastInstantiate(transform);
        } else if (id == 2)
        {
            temp = slashPool.FastInstantiate(transform);
        } else if (id == 3)
        {
            temp = bloodPool.FastInstantiate(transform);
        }

        temp.transform.position = position;

        return temp;
    }

    private static Vector3 RandomPointInBounds(Collider2D col)
    {
        return new Vector3(
            Random.Range(col.bounds.min.x, col.bounds.max.x),
            Random.Range(col.bounds.min.y, col.bounds.max.y),
            -1f
        );
    }

    /// <summary>
    /// Returns the id corresponding to the passed type. Returns -1 if invalid or not found.
    /// </summary>
    /// <param name="type">The particle type</param>
    /// <returns>the id of the particle. -1 if not found or invalid</returns>
    public static int GetParticleID(ParticleType type)
    {
        int selected = -1;
        switch (type)
        {
            case ParticleType.smoke:
                selected = 0;
                break;
            case ParticleType.metalClash:
                selected = 1;
                break;
            case ParticleType.strike:
                selected = 2;
                break;
            case ParticleType.blood:
                selected = 3;
                break;
        }
        return selected;
    }
    #endregion

}
