﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

/// <summary>
/// Sends analytics to the cloud
/// </summary>
public class AnalyticsManager : MonoBehaviour
{
    [Tooltip("Set to true to disable analytics while in the Unity Editor.")]
    public bool pauseAnalyticsInEditor = true;
    public static bool pauseInEditMode = true;
    #region per level analytics
    //PER LEVEL SEGMENT ANALYTICS
    private static float timeStarted = 0;
    public static int damageTaken { get; set; }
    public static int damageDealt { get; set; }
    private static int deaths;
    public static int kills { get; set; }
    public static int jumps { get; set; }
    public static int attacks { get; set; }
    public static int dashes { get; set; }
    public static bool segmentCompleted { get; set; }
    public static int deathsByKillVolume { get; set; }
    public static char currentSegment { get; set; }
    #endregion

    #region per play session analytics
    //PER ENTIRE PAY SESSION ANALYTICS
    private float perSessionSecondsPlayed;
    public static int perSessionNpcInteractions { get; set; }
    private static int perSessionDeaths;
    public static int perSessionMaxCombos { get; set; }
    public static int perSessionSegmentsCompleted { get; set; }
    public static int perSessionObjectsDestroyed { get; set; }
    #endregion

    private void Awake()
    {
        pauseInEditMode = pauseAnalyticsInEditor;
    }

    #region sends
    private void FixedUpdate()
    {
        perSessionSecondsPlayed += Time.fixedUnscaledDeltaTime;
    }
    private void OnDestroy()
    {
        if (!(Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor) || !pauseInEditMode)
        {
            if (perSessionSecondsPlayed > 60)
            {
                AnalyticsResult lastSend = AnalyticsEvent.Custom("play_session", new Dictionary<string, object>
                {
                    {"minutes_played", perSessionSecondsPlayed/60},
                    {"npc_interactions", perSessionNpcInteractions},
                    {"deaths", perSessionDeaths},
                    {"max_hit_combos", perSessionMaxCombos},
                    {"segments_completed", perSessionSegmentsCompleted},
                    {"objects_destroyed", perSessionObjectsDestroyed}
                });
                Debug.Log("Endgame analytics sent! " + lastSend);
            }
        }
    }
    public static void CompleteLevelSegment()
    {
        if (!(Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor) || !pauseInEditMode)
        {
            AnalyticsResult lastSend = AnalyticsEvent.Custom("complete_segment_" + currentSegment, new Dictionary<string, object>
            {
                {"kill_volume_deaths", deathsByKillVolume},
                {"minutes_taken", (Time.time - timeStarted)/60},
                {"damage_taken", damageTaken},
                {"damage_dealt", damageDealt},
                {"deaths", deaths},
                {"kills", kills},
                {"jumps", jumps},
                {"dashes", dashes},
                {"attacks", attacks},
                {"segment_completed", segmentCompleted}
            });
            Debug.Log("Segment analytics sent! " + lastSend);
            ResetSegmentStats();
        }
    }
    #endregion
    public static void StartNewLevelSegment(char segmentLetter)
    {
        segmentCompleted = true;
        CompleteLevelSegment();
        currentSegment = segmentLetter;
        perSessionSegmentsCompleted++;
    }
    public static void IncrementDeaths()
    {
        deaths++;
        perSessionDeaths++;
    }
    private static void ResetSegmentStats()
    {
        damageTaken = 0;
        damageDealt = 0;
        deaths = 0;
        kills = 0;
        jumps = 0;
        attacks = 0;
        dashes = 0;
        segmentCompleted = false;
        currentSegment = '!';
        timeStarted = Time.time;
    }
}