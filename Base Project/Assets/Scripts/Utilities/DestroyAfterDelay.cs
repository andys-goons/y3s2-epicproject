﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Destroys a game object after a certain amount of seconds.
/// </summary>
public class DestroyAfterDelay : MonoBehaviour
{
    public float timeToDestruction = 1;

    // Update is called once per frame
    void Update()
    {
        timeToDestruction -= Time.deltaTime;
        if(timeToDestruction <= 0)
        {
            Destroy(gameObject);
        }
    }
}
