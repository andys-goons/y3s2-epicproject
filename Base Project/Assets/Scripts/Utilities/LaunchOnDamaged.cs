﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Launches the GameObject when damaged. Useful for flipping tables.
/// </summary>
[RequireComponent(typeof(Rigidbody2D))]
public class LaunchOnDamaged : MonoBehaviour
{
    public Vector2 minLaunchForce;
    public Vector2 maxLaunchForce;
    public float minLaunchTorque;
    public float maxLaunchTorque;

    [Tooltip("The minimum amount of seconds the object waits between taking hits.")]
    public float hitCooldown;
    private float hitCooldownTimer;

    private Rigidbody2D rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    protected void FixedUpdate()
    {
        if (hitCooldownTimer >= 0)
        {
            hitCooldownTimer -= Time.fixedDeltaTime;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (hitCooldownTimer <= 0)
        {
            if (collision.gameObject.tag == "Attack")
            {
                rb.AddForce(new Vector2(Random.Range(minLaunchForce.x, maxLaunchForce.x), Random.Range(minLaunchForce.y, maxLaunchForce.y)), ForceMode2D.Impulse);
                rb.AddTorque(Random.Range(minLaunchTorque, maxLaunchTorque));
            }
        }
    }
}
