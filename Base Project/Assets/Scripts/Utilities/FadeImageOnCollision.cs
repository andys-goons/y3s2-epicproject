﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Collider2D))]
public class FadeImageOnCollision : MonoBehaviour
{
    [Tooltip("These will fade OUT when triggered.")]
    public SpriteRenderer[] images1;
    [Tooltip("These will fade IN when triggered.")]
    public SpriteRenderer[] images2;

    [Tooltip("How long it takes to fade")]
    public float fadeDuration = 0.5f;
    [Tooltip("The amount of steps to complete the fade. Higher numbers make it more smooth")]
    public float fadeSteps = 30;

    private bool toggle = false;
    private Color invis = new Color(1, 1, 1, 0);
    private Color normal = new Color(1, 1, 1, 1);

    public string targetTag = "Player";

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Hit something!...");
        if (collision.gameObject.tag == targetTag)
        {
            Debug.Log("Fading...");
            toggle = !toggle;
            StopAllCoroutines();
            StartCoroutine(StartFade());
        }
    }

    public IEnumerator StartFade()
    {
        for (int i = 0; i < fadeSteps; i++)
        {
            if (toggle)
            {
                for (int j = 0; j < images1.Length; j++)
                {
                    images1[j].color = Color.Lerp(normal, invis, i / fadeSteps);
                }
                for (int j = 0; j < images2.Length; j++)
                {
                    images2[j].color = Color.Lerp(invis, normal, i / fadeSteps);
                }
            } else
            {

                for (int j = 0; j < images1.Length; j++)
                {
                    images1[j].color = Color.Lerp(invis, normal, i / fadeSteps);
                }
                for (int j = 0; j < images2.Length; j++)
                {
                    images2[j].color = Color.Lerp(normal, invis, i / fadeSteps);
                }
            }
            Debug.Log("fadestep " + i / fadeSteps);
            yield return new WaitForSeconds(fadeDuration / fadeSteps);
        }

    }
}
