﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles what happens when ex is collected by the player.
/// </summary>
public class CollectEx : MonoBehaviour
{
    public PlayerController playerController;
    public PlayerAttackController playerAttackController;
    public GameObject attackInfoUI;
    public ToggleChildrenActive originalControlsUI;
    public GameObject ex;
    private void Awake()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        if (sr != null) sr.enabled = false;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            attackInfoUI.SetActive(true);
            playerAttackController.enabled = true;
            playerController.SetDashEnabled(true);
            originalControlsUI.Toggle();
            ex.SetActive(false);

            gameObject.SetActive(false);
        }
    }
}
