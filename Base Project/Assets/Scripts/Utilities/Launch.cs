﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Launches the game object on Start
/// </summary>
[RequireComponent(typeof(Rigidbody2D))]
public class Launch : MonoBehaviour
{
    [SerializeField] private Vector2 launchForceMin;
    [SerializeField] private Vector2 launchForceMax;
    [SerializeField] private float launchTorqueMin;
    [SerializeField] private float launchTorqueMax;

    private Rigidbody2D rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        Vector2 newLaunchForce = new Vector2(Random.Range(launchForceMin.x,launchForceMax.x), Random.Range(launchForceMin.y, launchForceMax.y));
        float newLaunchTorque = Random.Range(launchTorqueMin, launchTorqueMax);

        rb.AddForce(newLaunchForce, ForceMode2D.Impulse);
        rb.AddTorque(newLaunchTorque, ForceMode2D.Impulse);
    }
}
