﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A script that randomizes the sprite of a renderer on play. Selects the attached sprite renderer by default, but can be used for a renderer on a different object.
/// </summary>
public class RandomSprite : MonoBehaviour
{
    public Sprite[] sprites;

    [Tooltip("If left empty, it will select the renderer from this game object.")]
    public SpriteRenderer spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        if (spriteRenderer == null)
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }
        spriteRenderer.sprite = sprites[Random.Range(0,sprites.Length)];
    }

}
