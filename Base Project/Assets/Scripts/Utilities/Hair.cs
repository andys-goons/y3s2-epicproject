﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hair : MonoBehaviour
{
    //Editor
    public bool showlinks = false;
    public bool showTransforms = false;


    public float gravityFactor = -0.1f;
    public float maxDist = 0.5f;
    public float minDistPercent = 0.5f;
    public float minDist;
    public float accelerationFactor = 0.5f;
    private Vector3 lastPos;

    public List<HairInfo> hairInfo;

    private void Start()
    {
        lastPos = transform.position;
    }


    private void FixedUpdate()
    {
        for (int i = 0; i < hairInfo.Count; i++)
        {
            if (i == 0) continue; //Skip the first piece, it is the base
            Vector3 firstPos = hairInfo[i].hairPiece.position;//Get the initial position
            hairInfo[i].hairPiece.position += (lastPos - transform.position) * (0.7f / hairInfo[i].mass);
            hairInfo[i].hairPiece.position += hairInfo[i].acceleration * accelerationFactor * (1 / hairInfo[i].mass);//Apply add last frame's movement and multiply by a factor (careful for infinite movement after 1.2 factor)(considers piece's mass)
            hairInfo[i].hairPiece.position -= Vector3.down * gravityFactor * (1 + hairInfo[i].mass * 0.2f);//Apply some gravity (can be nagative)
            Vector3 dir = (hairInfo[i].hairPiece.position - hairInfo[i].linkedPiece.position);//Find the direction between this and the previous piece of hair
            hairInfo[i].hairPiece.position = hairInfo[i].linkedPiece.position + (dir.magnitude > maxDist ? dir.normalized * maxDist : dir.magnitude < minDist ? dir.normalized * minDist : dir);
            //if the piece is too far away from the link, bring it to its max distance, if it is below, bring it to its min distance (both in the appropriate direction). If it is in an acceptable position, leave it!
            //if that statement doesnt make sense, it is just a nested if/else. (if this is true)? return this : else ((if this is true)? return this: else return this) 
            hairInfo[i].acceleration = hairInfo[i].hairPiece.position - firstPos;//Find out how far this piece moved this frame
            hairInfo[i].acceleration += (lastPos - transform.position);//add some drag
            if (i != 1) hairInfo[i - 1].acceleration += hairInfo[i].acceleration * 0.3f;
            if (i != 1 && i != 2) hairInfo[i - 2].acceleration += hairInfo[i].acceleration * 0.2f;
            if (i != 1 && i != 2 && i != 3) hairInfo[i - 3].acceleration += hairInfo[i].acceleration * 0.1f;
        }
        lastPos = transform.position;
    }
}
[System.Serializable]
public class HairInfo
{
    public Vector3 acceleration = new Vector2();//How fast was it moving last frame
    public Vector3 momentum = new Vector3();
    public Transform hairPiece;//The moved object
    public Transform linkedPiece;//The object that the moved object moved relative to
    public float mass = 1;//how heavy is this (takes more to move it, not like real physics)
    public HairInfo(Transform piece, Transform linkedPiece)
    {
        hairPiece = piece;
        this.linkedPiece = linkedPiece;
    }
}