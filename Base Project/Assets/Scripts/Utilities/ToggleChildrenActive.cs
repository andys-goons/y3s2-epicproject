﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Toggles the children of this game object on or off when escape is pressed. Used for the tutorial GUI.
/// </summary>
public class ToggleChildrenActive : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Toggle();   
        }
    }
    public void Toggle()
    {
        foreach (Transform i in transform)
        {
            i.gameObject.SetActive(!i.gameObject.activeSelf);
        }
    }
}
