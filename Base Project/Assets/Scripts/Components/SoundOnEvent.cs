﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A simple script to play an FMOD event with a public function.
/// </summary>
public class SoundOnEvent : MonoBehaviour
{
    [FMODUnity.EventRef] public string soundToPlay;

    public void PlaySoundAuto()
    {
        PlaySound(soundToPlay);
    }

    public void PlaySound(string sound)
    {
        FMODUnity.RuntimeManager.PlayOneShot(sound);
    }
}
