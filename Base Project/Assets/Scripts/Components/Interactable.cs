﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public enum InteractableType { NPC, Object }

/// <summary>
/// Put this on a GameObject to allow the player to interact with it. Currently only player an FMOD event and/or particle system on interact.
/// </summary>
public class Interactable : MonoBehaviour
{
    //A static list of interactibles. Used by PlayerInteractor.
    public static List<Interactable> interactibles;

    [Tooltip("Used for analytics")]
    public InteractableType type = InteractableType.NPC;
    
    [Tooltip("The minimum time, in seconds, between interactions with this object.")]
    public float interactCooldownReset = 0.4f;
    private float interactCooldown;

    [Tooltip("The particle will be reset and played when this object is interacted with.")]
    public ParticleSystem interactParticle;
    [Tooltip("The FMOD event to play when this object is interacted with.")]
    [EventRef]
    public string interactSound;

    // Start is called before the first frame update
    void Start()
    {
        if (interactibles == null)
        {
            interactibles = new List<Interactable>();
        }
        //Add this to the list of interactibles.
        interactibles.Add(this);  
    }

    private void Update()
    {
        if (interactCooldown > 0)
        {
            interactCooldown -= Time.deltaTime;
        }
    }

    private void OnDestroy()
    {
        //Ensure this is removed from the list to keep it tidy and avoid errors.
        interactibles.Remove(this);
    }

    /// <summary>
    /// Causes an interaction with this object.
    /// </summary>
    /// <param name="interacter">The object that is interacting with this object.</param>
    /// <returns>True if successful, otherwise false.</returns>
    public bool Interact(PlayerInteracter interacter)
    {
        if (interactCooldown <= 0)
        {
            if (type == InteractableType.NPC) AnalyticsManager.perSessionNpcInteractions++;
            interactCooldown = interactCooldownReset;

            if (interactParticle) interactParticle.Play();
            if (interactSound != "") RuntimeManager.PlayOneShotAttached(interactSound, gameObject);
            return true;
        }
        return false;
    }
}
