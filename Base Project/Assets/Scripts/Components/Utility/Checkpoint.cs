﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Checkpoint is a script that marks a collider as a checkpoint. This contains data about the point, and disables the sprite renderer on play for ease of
/// setting up in editor, without having to disable manually.
/// </summary>

[RequireComponent(typeof(Collider2D))]
public class Checkpoint : MonoBehaviour
{
    [Tooltip("The letter of the level segment the player is entering when they hit this checkpoint. Ignored if already visited")]
    public char segmentEntering = 'A';
    [Tooltip("Determines if the checkpoint has been visted or not yet. Only used for segment tracking.")]
    public bool visited = false;
    [Tooltip("Set to true if the player spawns on this checkpoint on level start. Prevents Unity Analytics from firing.")]
    public bool spawnCheckpoint = false;
    [Tooltip("Set to true if the checkpoint does NOT start a new level segment.")]
    public bool intermittantCheckpoint;

    private void Awake()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        if (sr != null) sr.enabled = false;
    }
}
