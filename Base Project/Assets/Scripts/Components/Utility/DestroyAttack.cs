﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAttack : MonoBehaviour
{
    public string destroyTag;
    public int enemyLayer;
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == destroyTag && collision.gameObject.layer == enemyLayer)
        {
            Destroy(collision.gameObject);
        }
    }
}
