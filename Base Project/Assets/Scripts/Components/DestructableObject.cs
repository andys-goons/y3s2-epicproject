﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

/// <summary>
/// A script for objects that can be attacked and destroyed.
/// </summary>
public class DestructableObject : MonoBehaviour
{
    [Header("Destruction Events")]

    [Tooltip("Should this Game Object destsroy itself on death?")]
    [SerializeField] protected bool destroyOnDeath = true;
    [Tooltip("The prefab to instantiate on death. Ignored if left blank.")]
    [SerializeField] protected GameObject deathPrefab;

    [Tooltip("The function name to call from the target object when this object's hp reaches zero. Ignored if left blank.")]
    [SerializeField] protected string destructionSendMessage;
    [Tooltip("The target object of Destruction Send Message")]
    [SerializeField] protected GameObject messageTarget;

    [Tooltip("How long time should be slowed when this object is destroyed.")]
    [SerializeField] protected float slowTimeOnDeathDuration;
    [EventRef] [SerializeField] protected string deathSoundEvent;

    [Header("Statistics")]

    [SerializeField] protected bool invulnerable = false;

    [Tooltip("The object's hit points")]
    [SerializeField]  public int hp;
    [SerializeField]  public int maxHp;

    [Tooltip("The minimum amount of seconds the object waits between taking damage. Damage is not queued.")]
    [SerializeField] protected float damageCooldown;
    [SerializeField] protected float damageCooldownTimer;

    private void Start()
    {
        hp = maxHp;
    }
    protected void FixedUpdate()
    {
        if (damageCooldownTimer >= 0)
        {
            damageCooldownTimer -= Time.fixedDeltaTime;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.gameObject.tag == "Attack")
        {
            Damage(1);
        }
    }

    /// <summary>
    /// Deal damage to the object.
    /// </summary>
    /// <param name="amount">The amount of damage the object should take</param>
    /// <returns>True if the object took damage, else returns false</returns>
    public bool Damage(int amount)
    {
        //if the timer is not currently blocking incoming damage, take damage and return true
        if (damageCooldownTimer <= 0 && !invulnerable)
        {
            hp = Mathf.Clamp(hp -= amount, 0, maxHp);

            AnalyticsManager.damageDealt += amount;

            damageCooldownTimer = damageCooldown;

            Juicer.Instance.StrikeEffect(GetComponent<Collider2D>());

            CheckAlive();
            return true;
        }
        return false;
    }

    /// <summary>
    /// Checks if the object's hp is larger than 0. If not, initiate destruction.
    /// </summary>
    /// <returns>true if the object has hp remaining, else false</returns>
    private bool CheckAlive()
    {
        if (hp > 0)
        {
            return true;
        }
        else
        {
            AnalyticsManager.perSessionObjectsDestroyed++;

            //If a message has been defined, send it
            if (destructionSendMessage != "") messageTarget.SendMessage(destructionSendMessage);

            //If there's a prefab to instantiate, do it
            if (deathPrefab != null) Instantiate(deathPrefab, transform.position, Quaternion.identity);

            if (!string.IsNullOrEmpty(deathSoundEvent)) RuntimeManager.PlayOneShotAttached(deathSoundEvent, gameObject);

            if (destroyOnDeath) Destroy(gameObject);
            return false;
        }
    }
}
