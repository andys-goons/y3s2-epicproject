﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A script used so that fastPool won't freak out when trying to use SendMessage()
/// </summary>
public class SuppressOnFastDestroy : MonoBehaviour
{
    public void OnFastDestroy()
    {
        return;
    }
}
