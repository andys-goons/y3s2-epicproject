﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A component to go on the player GameObject. Allows them to interact with Interactibles. 
/// Change the control for interact in the Unity input manager for "Interact"
/// </summary>
public class PlayerInteracter : MonoBehaviour
{
    [Tooltip("The distance an interactable must be within to be interacted with.")]
    public float interactRange = 2f;

    private bool interactKeyPressed = false;

    private void Update()
    {
        //Get the interact key. Added check so it only fires on the frame the key was pressed.
        if (Input.GetAxisRaw("Interact") == 1 && !interactKeyPressed)
        {
            interactKeyPressed = true;

            //Loop through the current interactible objects. Interact with those in range.
            foreach(Interactable i in Interactable.interactibles)
            {
                if((transform.position - i.transform.position).magnitude < interactRange)
                {
                    i.Interact(this);
                }
            }
        } 
        if (Input.GetAxisRaw("Interact") == 0)
        {
            interactKeyPressed = false;
        }
    }
}
