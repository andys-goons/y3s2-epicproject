﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Apply to a GameObject to make its collider hurt the player. MUST BE A TRIGGER COLLIDER!
/// </summary>

[RequireComponent(typeof(Collider2D))]
public class DamageVolume : MonoBehaviour
{
    [Tooltip("Set to true to instantly kill the player regardless of their HP.")]
    public bool instantKill = false;
    [Tooltip("The amount of damage this volume does to the player.")]
    public int damagePower = 1;

    private void Awake()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        if(sr != null) sr.enabled = false;
    }
}
