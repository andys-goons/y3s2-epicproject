﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

/// <summary>
/// Inherits from DestructibleObject. The base class for enemies. If some logic applies to all types of enemies, put it in here!
/// </summary>
public class Enemy : DestructableObject
{
    [Tooltip("The amount of damage this enemy does to the player.")]
    [SerializeField] protected int strength = 1;
    [Tooltip("The sight range in unity units.")]
    [SerializeField] protected float sightRange = 4;
    [Tooltip("The GameObject that this enemy is trying to kill")]
    [SerializeField] protected GameObject target;

    [Tooltip("The FMOD event to play as an idling sound")]
    [EventRef] [SerializeField] protected string idleSoundEvent;
    [SerializeField] protected float delayBetweenIdleSounds = 7;
    private float idleSoundTimer;

    [Tooltip("The FMOD Event to play when attacking")]
    [EventRef] [SerializeField] protected string attackSoundEvent;

    private void Awake()
    {
        idleSoundTimer = delayBetweenIdleSounds;
    }
    private void Update()
    {
        //Idling sound logic. Plays an idle sound when the player is withing sight range x 2
        if (!string.IsNullOrEmpty(idleSoundEvent) && (target.transform.position - transform.position).magnitude < sightRange * 2f)
        {
            if (idleSoundTimer <= 0)
            {
                RuntimeManager.PlayOneShotAttached(idleSoundEvent, gameObject);
                idleSoundTimer = Random.Range(delayBetweenIdleSounds * 0.7f, delayBetweenIdleSounds * 1.3f);
            }
            else
            {
                idleSoundTimer -= Time.deltaTime;
            }
        }
    }

    new public bool Damage(int amount)
    {
        if (hp < 1) AnalyticsManager.kills++;
        return base.Damage(amount);
    }
    public int GetStrength()
    {
        return strength;
    }
}
