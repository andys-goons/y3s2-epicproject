﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// A simple script to call an event when trigger is entered
/// </summary>
[RequireComponent(typeof(Collider2D))]
public class NewMusicOnTrigger : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string newMusicEvent;

    public string targetTag = "Player";

    private void Awake()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        if (sr != null) sr.enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == targetTag) MusicManager.instance.PlayNewMusic(newMusicEvent);
    }
}
