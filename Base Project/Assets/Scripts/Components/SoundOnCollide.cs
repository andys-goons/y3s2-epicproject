﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Plays an FMOD event on particle collision and on collision. NOT on trigger!
/// </summary>
public class SoundOnCollide : MonoBehaviour
{
    public bool enableParticleCollision;
    public bool enableRegularCollision;

    [FMODUnity.EventRef] public string collisionSound;
    private void OnParticleCollision(GameObject other)
    {
        if (enableParticleCollision)
        {
            FMODUnity.RuntimeManager.PlayOneShotAttached(collisionSound, gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (enableRegularCollision)
        {
            FMODUnity.RuntimeManager.PlayOneShotAttached(collisionSound, gameObject);
        }
    }
}
